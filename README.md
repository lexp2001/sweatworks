# Sweatworks - Test Fullstack - Frontend 


## Online Version

http://sweatworks.ml


## Clone the repository using HTPPS protocol

git clone https://lexp2001@bitbucket.org/lexp2001/sweatworks.git


## Install dependencies

cd sweatworks/

npm install


## Development server

Run `ng serve` for a dev server. Your port 4200 must to be free for this test

ng serve


## Open the app on the browser

http://localhost:4200/


## Testing the app

Go to the last author (Lex Phulop) click on his row and check his publications

Check the other functions of the application


### Notes

This is a very simple APP implementation, in a real scenario, it would be necessary to add some other functionalities such as error handling, fields validations, confirmation dialogs before deletion, disable buttons in those cases that are not convenient to use, validation of dates, emails, integrity checking in storage, etc.