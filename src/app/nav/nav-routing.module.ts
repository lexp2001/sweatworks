import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Routes Components
import { BookComponent } from '../sweatworks/book/book.component'; // Selected Publication
import { BooksComponent } from '../sweatworks/books/books.component'; // Publications List
import { AuthorComponent } from '../sweatworks/author/author.component'; // Selected Autor

// Routes for all interfaces
const routes: Routes = [
  {
    path: 'sweatworks',
    component: BooksComponent,
    data: {
      title: 'Publications of '
    }
  },{
    path: 'sweatworks/create_author',
    component: AuthorComponent,
    data: {
      title: 'Current Author: '
    }
  },{
    path: 'sweatworks/edit_author',
    component: AuthorComponent,
    data: {
      title: 'Edit Author',
      edit: true
    }
  },
  {
    path: 'sweatworks/create_book',
    component: BookComponent,
    data: {
      title: 'Create Publication'
    }
  },
  {
    path: 'sweatworks/edit_book',
    component: BookComponent,
    data: {
      title: 'Edit Publication',
      edit: true
    }
  },
  {
    path: '',
    redirectTo: 'sweatworks',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class NavRoutingModule { }
