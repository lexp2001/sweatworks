import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgModule } from '@angular/core';
import { NavRoutingModule } from './nav/nav-routing.module';
import { DataShareService } from "./sweatworks/services/share.service";
import { HttpClientModule } from '@angular/common/http';

//3party Modules 
import { AngularFontAwesomeModule } from 'angular-font-awesome';

//Material Comnponents
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule, MatNativeDateModule } from '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatListModule } from '@angular/material/list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material';
import { MatMenuModule } from '@angular/material/menu';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDatepickerModule } from '@angular/material/datepicker';

//App Components
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LOCALE_ID } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

// Main sweatworks components
import { AuthorComponent } from './sweatworks/author/author.component';
import { BookComponent } from './sweatworks/book/book.component';
import { BooksComponent } from './sweatworks/books/books.component';

@NgModule({
  declarations: [
    AppComponent,

    // Sweatworks
    AuthorComponent,
    BookComponent,
    BooksComponent
    
  ],
  imports: [

    BrowserModule,
    BrowserAnimationsModule,
    
    // NoopAnimationsModule,
    NavRoutingModule,
    MatCardModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatPaginatorModule,
    MatGridListModule,
    MatListModule,
    MatSnackBarModule,
    MatDialogModule,
    MatIconModule,
    MatMenuModule,
    
    FormsModule,
    HttpClientModule,

    AngularFontAwesomeModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
    
  ],
  providers: [ DataShareService, { provide: LOCALE_ID, useValue: 'en-GB' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
