// Author data structure
export class Author {
    id?: string;
    name: string;
    email: string;
    birthday: string;
}