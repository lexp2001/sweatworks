// Data for share between components
import { Author } from "../classes/author";
import { Publication } from "../classes/publication";

export class Share {
    title: string;
    author:Author;
    publication:Publication;
}