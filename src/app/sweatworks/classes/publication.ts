// Publication data structure
export class Publication {
    id?: string;
    id_author: string;
    date: string;
    title: string;
    body: string;
}