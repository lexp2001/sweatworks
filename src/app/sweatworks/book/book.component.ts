import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataShareService } from "../services/share.service"; // For share data between components
import { DataService } from "../services/data.service"; // Main services
import { Publication } from "../../sweatworks/classes/publication"; // Publications data structure
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css', '../../app.component.css']
})
export class BookComponent implements OnInit {

  public subsData: Subscription; // To keep the information of authors and publications updated dynamically
  public publication: Publication;
  public minDate = new Date(1900, 0, 1); // Min and max publications´ creation
  public maxDate = new Date(2018, 9, 11);
  public startDate = new Date(2018, 0, 1);
  public cardTitle = "New Publication";
  public hourAndMin = {date:"",hour:0,min:0};
  public editing = false; // May be "New Publication" or "Edit Publication"
  public authorId: string; // Id of current author

  constructor(
    private shareService: DataShareService,
    private dataService: DataService,
    private routes: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  clearPublication() {
    this.publication = { id_author:"", date:"", title:"", body:""};
  }

  // Transform date creation for date pickup compatibility without time
  updateSimpleDateToString( dateString ) {

    let newDate = new Date(dateString)
    
    let dd: number | string = newDate.getDate() + 1;
    if (dd < 10) {
      dd = '0' + dd;
    }
    let MM: number | string = newDate.getMonth() + 1;
    if (MM < 10) {
      MM = '0' + MM;
    }

    const yy: number = newDate.getFullYear();
    let MyDateString = `${yy}-${MM}-${dd}`;    
    
    return MyDateString;
    }

  // Transform date creation for date pickup compatibility with time support
  updateDateToString() {
    let newDate = new Date(this.hourAndMin.date)
    
    let dd: number | string = newDate.getDate();
    if (dd < 10) {
      dd = '0' + dd;
    }
    let MM: number | string = newDate.getMonth() + 1;
    if (MM < 10) {
      MM = '0' + MM;
    }

    let hh: number | string = this.hourAndMin.hour;
    if (hh < 10) {
      hh = '0' + hh;
    }
    let mm: number | string = this.hourAndMin.min;
    if (mm < 10) {
      mm = '0' + mm;
    }

    const yy: number = newDate.getFullYear();
    let MyDateString = `${yy}-${MM}-${dd}T${hh}:${mm}`;    
    
    return MyDateString;
    }

  // Save publication
  onClickSave() {

    let fixedDate = this.updateDateToString();
    this.publication.date = fixedDate;

    if ( !this.editing ) { // Is it a creation?

      this.publication.id_author = this.authorId;
      this.dataService.createPublication( this.publication).
        then (resp => {
          setTimeout(() => { 
            this.routes.navigate(['/sweatworks']);
          }, 1000);
        })
        
        
    } else { // It is an edition 

      this.dataService.updatePublication( this.publication).
        then (resp => {
          setTimeout(() => { 
            this.routes.navigate(['/sweatworks']);
          }, 1000);
        })
    }
  }

  // Deletes selected publications
  onClickDelete() {
    this.dataService.deletePublication( this.publication.id).
        then (resp => {
          setTimeout(() => { 
            this.routes.navigate(['/sweatworks']);
          }, 1000);
        })
  }

// Main Process
ngOnInit() {
  this.clearPublication();
  this.activatedRoute.data.subscribe(params => {
    this.shareService.setData({title:params.title});

      this.subsData = this.shareService.currentData.subscribe( resp =>{
        
        this.authorId = resp.author.id;
        if (params.edit) { // When editing
          this.publication = resp.publication;
          // Date time treatment
          this.hourAndMin.hour =  new Date(resp.publication.date).getHours();
          this.hourAndMin.min =  new Date(resp.publication.date).getMinutes();
          this.hourAndMin.date = this.updateSimpleDateToString( this.publication.date );
          this.cardTitle = "Edit Publication";
          this.editing = true;
        }
      });
  });
}

}
