import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { DataShareService } from "../services/share.service"; // Service for share information between components
import { DataService } from "../services/data.service"; // Main data services
import { Publication } from "../classes/publication"; // Publication data structure
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  public subsData: Subscription;
  public msgEmpty: string;

  // MatPaginator Inputs
  public length = 100;
  public pageSize = 5;
  public pageSizeOptions = [5, 10, 20, 50, 100];
  public currentPage = 0;
  public currentOrder = "date";
  public paginate = {
    "id":1,
    "limit": 5,
    "pag": 0,
    "search":"",
    "order":"date"
  };

  constructor(
    private shareService: DataShareService,
    private dataService: DataService,
    private routes: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  displayedColumns = ['title', 'date', 'go'];
  dataSource = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  // Get paginated publications by author's id
  getPagPublications( id ) {
    this.paginate.id = id;
    this.dataService.getPagPublications( this.paginate )
      .then( items => {
        this.length = items.total;
        this.dataSource = items.publications;
    });
  }

  // Go to selected publication for edition
  onClickEdit(publication:Publication){
    this.shareService.setData({publication:publication});
    this.routes.navigate(['sweatworks/edit_book']);
  }

  // Each paginator change
  pagChange( pageEvent ) {

    this.currentPage = pageEvent.pageIndex;
    this.pageSize= pageEvent.pageSize;

    this.paginate = { // Keep the paginator options
      "id":this.paginate.id ,
      "limit": this.pageSize,
      "pag": this.currentPage,
      "search":"",
      "order": this.currentOrder
    };
    this.getPagPublications( this.paginate.id );
  }

  // When user click on column headers for set the data order visualization
  changeOrder( order ) {
    this.currentOrder = order;
    this.paginate.order = order;
    this.getPagPublications( this.paginate.id );
  }

  // Set the searching filter
  applyFilter (searchWords) {
    this.paginate.search = searchWords;
    this.getPagPublications( this.paginate.id );
  }

  // Main process
  ngOnInit() {

    // Initialize pagination 
    this.paginator._intl.itemsPerPageLabel = 'Item per page:';
    this.paginator._intl.getRangeLabel= (page: number, pageSize: number, length: number) => { if (length == 0 || pageSize == 0) { return `0 de ${length}`; } length = Math.max(length, 0); const startIndex = page * pageSize; const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize; return `${startIndex + 1} - ${endIndex} de ${length}`; }
    
    // Set the page titles
    this.activatedRoute.data.subscribe(params => {
      this.shareService.setData({title:params.title});

    });

    // Keep information updated between components
    this.subsData = this.shareService.currentData.subscribe( resp =>{
      if ( resp.author !== null) {
        this.getPagPublications( resp.author.id );
      } else { 
        this.dataService.getAuthors()
          .subscribe( items => {
            this.shareService.setData({author:items[ 0 ]});
        });

      }
    });

  }

}
