// Main service file for API consumming
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { settings } from '../settings';// Global Main Settings, in production this file must to be editable 
import { Author } from "../../sweatworks/classes/author"; // Author data structure
import { Publication } from "../../sweatworks/classes/publication"; // Publication data structure

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'  })
};


@Injectable({
  providedIn: 'root'
})
export class DataService {

    //Get base URL from Settings file
    apiUrlBase = settings.apiUrlBase;

  constructor( private http: HttpClient ) { }

  /***************************************************************  AUTHOR´S SERVICES ***************************************************/

    // Get authors 
    
    getAuthors (){
        return <any> this.http.get(this.apiUrlBase + 'authors/showAuthors');
    }

    // Create one author
    /*
    getAuthors ( ): Promise<Author> {
        let url = this.apiUrlBase + 'authors/showAuthors';
        return <any> this.http
            .post( url ,{}, httpOptions)
            .toPromise();
    }
*/
    // Create one author
    createAuthor ( author:Author ): Promise<Author> {
        let url = this.apiUrlBase + 'authors/createAuthor';
        return <any> this.http
            .post( url ,author, httpOptions)
            .toPromise();
    }

    // Update specific author
    updateAuthor( author:Author ): Promise<Author> {
        let url = this.apiUrlBase + 'authors/updateAuthor' ;
        return  <any>this.http
            .put( url , author, httpOptions)
            .toPromise();
      }
    
    // Delete author by id and all it´s publications
    deleteAuthor ( id ): Promise<Publication> {
        let url = this.apiUrlBase + 'authors/deleteAuthor/' + id ;
        return  <any>this.http
            .delete( url , httpOptions)
            .toPromise();
    }

    /***************************************************************  PUBLICATION`S SERVICES ***************************************************/

    // Get Publications by author id
    
    getPublications ( id ){
        return <any> this.http.get(this.apiUrlBase + 'publications/showPublications/'+id);
    }
 
    // Get Publications by paginated options
    getPagPublications ( data ){
        let url = this.apiUrlBase + 'publications/showPagPublications';
        return <any> this.http
            .post( url ,data, httpOptions)
            .toPromise();
    }

    // Create a new publication
    createPublication ( publication:Publication ): Promise<Publication> {

        let url = this.apiUrlBase + 'publications/createPublication';
        return <any> this.http
            .post( url ,publication, httpOptions)
            .toPromise();
    }

    // Update a publication
    updatePublication( publication:Publication ): Promise<Publication> {
        let url = this.apiUrlBase + 'publications/updatePublication' ;
        return  <any>this.http
            .put( url , publication, httpOptions)
            .toPromise();
    }

    // Drop a publication by id
    deletePublication ( id ): Promise<Publication> {
        let url = this.apiUrlBase + 'publications/deletePublication/' + id ;
        return  <any>this.http
            .delete( url , httpOptions)
            .toPromise();
    }

    handleError: (error: Response) => {
        //
    }

}
