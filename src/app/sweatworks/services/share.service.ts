// Manager of share content
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { settings } from '../settings';// Global Main Settings

import { Share } from '../classes/share';
import { Router } from '@angular/router';

@Injectable()

export class DataShareService {

    // Data to be shared
    public shareData: Share = {title:"", author:null, publication: null};


    // Share  Data
    //********************************************************************************************* */

    private dataSource = new BehaviorSubject<Share>({title:"Sweatworks",author:null, publication: null});
    currentData = this.dataSource.asObservable();

    // Update Current Data to out
    setData (data){
        this.shareData = this.dataSource.value;
        if ( data.title ) this.shareData.title = data.title;
        if ( data.author ) this.shareData.author = data.author;
        if ( data.publication ) this.shareData.publication = data.publication;
        this.dataSource.next(this.shareData);
    }


    apiUrlBase = settings.apiUrlBase;

    constructor( 
        private routes: Router

    ) { }


}