import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataShareService } from "../services/share.service"; // For share data between components
import { DataService } from "../services/data.service"; // Main services
import { Author } from "../../sweatworks/classes/author"; // Author data structure
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css', '../../app.component.css']
})
export class AuthorComponent implements OnInit {

  public subsData: Subscription; // To keep the information of authors and publications updated dynamically
  public author: Author; 
  public minDate = new Date(1900, 0, 1); // Min and max author´s birthday 
  public maxDate = new Date(2010, 0, 1);
  public startDate = new Date(1990, 0, 1);
  public cardTitle = "New Author"; // May be "New Author" or "Edit Author"


  public editing = false; // True when editing selected author

  constructor(
    private shareService: DataShareService,
    private dataService: DataService,
    private routes: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  clearAuthor() {
    this.author = { name:"", email:"", birthday:""};
    
  }

  // Saves author or create a new one
  onClickSave() {
    if ( !this.editing ) {
      this.dataService.createAuthor( this.author).
        then (resp => {
          setTimeout(() => { 
            this.routes.navigate(['/sweatworks']);
          }, 1000);
        })
    } else {
      this.dataService.updateAuthor( this.author).
        then (resp => {
          setTimeout(() => { 
              this.routes.navigate(['/sweatworks']);
          }, 1000);
        })
    }
  }

  // Delete selected author
  onClickDelete() {
    this.dataService.deleteAuthor( this.author.id).
        then (resp => {
          console.info("deleteAuthor " + this.author.id, resp);
          
          setTimeout(() => { 
            //this.shareService.setData({author:null});
            this.routes.navigate(['/sweatworks']);
          }, 1000);
        })
  }

  // Main procedures
  ngOnInit() {
    this.clearAuthor();
    this.activatedRoute.data.subscribe(params => {
      console.info("activatedRouter:", params);
      this.shareService.setData({title:params.title});
      if (params.edit) { // True when editing, false when creating
        this.subsData = this.shareService.currentData.subscribe( resp =>{
          console.info("currentData:", resp);
          this.author = resp.author;
          this.cardTitle = "Edit Author";
          this.editing = true;
        });
        
      }
    });
  }

}
