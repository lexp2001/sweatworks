import { Component, OnInit } from '@angular/core';
import { DataShareService } from "./sweatworks/services/share.service"; // Share data between components
import { DataService } from "./sweatworks/services/data.service"; // Main services for API
import { Author } from "./sweatworks/classes/author"; // Author data structure
import { Subscription } from 'rxjs/Subscription'; 
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  public subsData: Subscription;
  public mainTitle: string;
  public authors: Author;
  public nameAuthor: string;
  public selectedAuthor: Author;

  constructor( 

    private shareService: DataShareService,
    private dataService: DataService,
    private routes: Router
    ) {}

    // Go to selected author
    onClickAuthor( author: Author){
      this.shareService.setData({author:author});
      this.routes.navigate(['/sweatworks/']);
    }

    // Initialize author data
    clearAuthor(){
      this.selectedAuthor = {
        "name":"",
        "email":"",
        "birthday":""
      }
    }

    // Create a new author
    createAuthor() {
      this.routes.navigate(['/sweatworks/create_author']);
    }

    // Main process
    ngOnInit() {
      this.clearAuthor();
      this.subsData = this.shareService.currentData.subscribe( resp =>{
        this.mainTitle = resp.title;
        if( resp.author !== null) {
          this.nameAuthor = resp.author.name;
        }
        // Consuming authors' data
        this.dataService.getAuthors()
          .subscribe( items => {
            this.authors = items;
        });
        
      });
      
    }

}